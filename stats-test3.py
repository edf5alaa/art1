#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys

import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm


if __name__ == '__main__':
    X, Y, Z = [], [], []
    Z1, Z2 = [], []
    for line in sys.stdin:
        dct = eval(line)
        mode, noize = dct['mode']
        x, y = dct['tr_vig'], dct['ret_vig']
        ntests, nfails, nunkowns = [dct[key] for key in ['ntests', 'nfails', 'nuknowns']]
        accuracy = (1 - (nfails + nunkowns) / ntests) * 100

        X.append(x)
        Y.append(y)
        Z.append(accuracy)
        Z1.append((1 - nfails / ntests) * 100)
        Z2.append((1 - nunkowns / ntests) * 100)
    
    # ax.set_zlim(-0.01, 100.01)

    Title = ['total faults', 'nb fails', 'nb unkowns']
    
    for i, Zaxis in enumerate([Z, Z1, Z2]):
        fig = plt.figure(i)
        
        ax = fig.add_subplot(111, projection='3d')
        ax.plot_trisurf(X=X, Y=Y, Z=Zaxis, cmap=cm.coolwarm, linewidth=0, antialiased=True)
        plt.title('mode {}, noize={} : {}'.format(mode, noize, Title[i]))
        ax.set_xlabel('training vigilance')
        ax.set_ylabel('retrieving vigilance')
        ax.set_zlabel('accuracy')

    plt.show()
