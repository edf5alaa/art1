#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# python 3.2
# josué melka
# adapté à partir du code de M. Jym Feat


import random
from collections import deque
from operator import itemgetter

findex = itemgetter(0)
fvalue = itemgetter(1)

def magnitude(V):
    return V.count(1)

class ART1:
    def __init__(self, N, vigilance):
        self.N = N
        self.vigilance = vigilance
        self.initB = 1. / (N + 1)
        self.M = 0
        self.T, self.B = [], []
        self.newstar()

    def newstar(self):
        self.T.append([True] * self.N)
        self.B.append([self.initB] * self.N)
        self.M += 1
        return self.M - 1

    def process(self, inputs):
        stable = True                           # pretend this run will be stable
        for X in inputs :                       # scan input forms
            SXi = magnitude(X)                  # compute number of 1's
            mapped = map(lambda j: (j, sum(self.B[j][i] * X[i] for i in range(self.N))), range(self.M))
            Activations = deque(sorted(mapped, key=fvalue, reverse=True))   # compute all activations once
            resonance = False                   # null echo yet
            while not resonance:                # loop till it resonates
                try:
                    star = Activations.popleft()[0]     # get the best remained attractor
                except IndexError:
                    star = self.newstar()       # unless none found: allocate new one
                C = [self.T[star][i] & X[i] for i in range(self.N)]   # print in short-term memory    ### euh ???
                SCi = magnitude(C)              # compute number of 1's
                if (SCi / SXi) < self.vigilance:
                    continue                    # too bad, restart
                resonance = True                # okay, proper echo
                if self.T[star] != C:           # does our star need an update? ### why C ?
                    self.update(star, C, SCi)   # do just that
                    stable = False              # altered state
        return stable

    def update(self, x, C, SigmaCi):
        for i in range(self.N):
            self.T[x][i] = C[i]
            self.B[x][i] = C[i] * (2 / (1 + SigmaCi))
            
    def run(self, inputs):
        run = 1
        while not self.process(random.sample(inputs, len(inputs))):
            print('  pass {} ...'.format(run))
            run += 1
        print('  used', self.M, 'attractors')

    def retrieve(self, X, vigilance):
        SXi = magnitude(X)
        mapped = map(lambda j: (j, sum(self.B[j][i] * X[i] for i in range(self.N))), range(self.M))
        Activations = deque(sorted(mapped, key=fvalue, reverse=True))
        while True:
            try:
                star = Activations.popleft()[0]
            except IndexError:
                return None
            C = [self.T[star][i] & X[i] for i in range(self.N)]
            SCi = magnitude(C)
            if (SCi / SXi) < vigilance:
                continue
            return SCi / SXi


#################### for testing purpose #####################

def bits(BitList):
    return ''.join('1' if b else '0' for b in BitList)

def make_samples(start):
    return [bin(b)[2:] for b in range(start, 0, -1)] 

def binform(samples):
    inputs = [[x is '1' for x in input] for input in samples]
    N = max(len(input) for input in inputs)
    for x in range(len(inputs)):
        inputs[x] = [False] * (N - len(inputs[x])) + inputs[x]
    return inputs


if __name__ == '__main__':
# ~~~~~~~ global data
    inputs = binform(make_samples(512)) # please test this with 3, 4, ..., 1024
    N = len(inputs[0]) # sensors
    vigilance = (N - 1) * (1. / N) + 1. / 10000 # hack
    art1 = ART1(N, vigilance)
    art1.run(inputs)
