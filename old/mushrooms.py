#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import time
import random

from itertools import count

from predictive_art import PredART


N = 22


def get_data(filename):
    ret = []
    with open(filename, 'r') as fp:
        for line in fp:
            elems = line.strip().split(',')
            if len(elems) == N + 1:
                ret.append(elems)
    return ret

def make_vec(instance, dct):
    ret = [0] * len(dct)
    for i, attr in enumerate(instance):
        if attr == '?': continue
        ret[dct[i, attr]] = 1
    return ret

def vec2mushroom(vec, rev_dct):
    return [rev_dct[i][1] for i, v in enumerate(vec) if v]

def get_vectors(data):
    domains = [set() for _ in range(N)]
    
    for categ, *inst in data:
        for s, e in zip(domains, inst):
            if e != '?': s.add(e)    

    print(sum(len(s) for s in domains), 'attributes in total')

    gen = count()
    dct = {(i, attr): next(gen) for i, s in enumerate(domains) for attr in s}

    return [(categ, make_vec(inst, dct)) for categ, *inst in data]

# efface uniquement des informations
def noize(vec, k):
    vec = vec[:]    # copy the vector
    idx = [i for i, v in enumerate(vec) if v]
    assert k < len(idx)
    for i in random.sample(idx, k):
        vec[i] = False
    return vec

# ajoute de fausses informations
def noize2(vec, k):
    vec = vec[:]        # copy the vector
    assert k < len(vec)
    for i in random.sample(range(len(vec)), k):
        vec[i] ^= True  # toggle value
    return vec


if __name__ == '__main__':
    if len(sys.argv) != 2:
        print('usage:', sys.argv[0], '<infile>', file=sys.stderr)
        exit(1)

    t = time.time()
    data = get_data(sys.argv[1])
    print('{} instances proceed, in {:.3} s'.format(len(data), time.time() - t))

    test = get_vectors(random.sample(data, 800))

    p_art1 = PredART()
    p_art1.train(test, 0.8)

    K = 250
    noise = 30
    vigilance = 0.3
    nfails = 0
    nunkowns = 0
    for categ, vec in random.sample(test, K):
        #print(vec); input('continuer ?')
        ret = p_art1.retrieve(noize2(vec, noise), vigilance)
        if ret == 'unkown':
            nunkowns += 1
        elif ret != categ:
            nfails += 1
    print('  vigilance={}, noise={} : {} fails ({:.2%}), {} not recognized ({:.2%}), accuracy {:.2%}'.\
                    format(vigilance, noise, nfails, nfails / K, nunkowns, nunkowns / K, 1 - (nfails + nunkowns) / K))
