#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from art1 import ART1

from collections import defaultdict

class PredART:
    def train(self, data, vigilance):
        self.vigilance = vigilance
    
        categories = defaultdict(list)      
        for categ, vec in data:
            categories[categ].append(vec)
            
        self.art1_system = {categ: ART1(len(vlst[0]), vigilance) for categ, vlst in categories.items()}
        for categ, vlst in categories.items():
            print('training for "{}" categorie ({} items)'.format(categ, len(vlst)))
            self.art1_system[categ].run(vlst)

    def retrieve(self, vec, vigilance=None):
        if vigilance is None: vigilance = self.vigilance
        lst = []
        for categ, art1 in self.art1_system.items():
            accuracy = art1.retrieve(vec, vigilance)
            if accuracy:
                lst.append((categ, accuracy))
        if len(lst) > 1:
            return max(lst, key=lambda t: t[1])[0]
        elif len(lst) == 0:
            return 'unkown'
        return lst[0][0]
