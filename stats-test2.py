#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import matplotlib.pyplot as plt


if __name__ == '__main__':
    Mode = {i: [] for i in [0, 1, 2]}
    for line in sys.stdin:
        dct = eval(line)
        tr_vig, ret_vig = dct['tr_vig'], dct['ret_vig']
        
        mode, noize = dct['mode']
        ntests, nfails, nunkowns = [dct[key] for key in ['ntests', 'nfails', 'nuknowns']]
        accuracy = (1 - (nfails + nunkowns) / ntests) * 100
        Mode[mode].append((noize, accuracy))

    plt.figure()
    
    sp = {1: 211, 2: 212}
    col = {1: 'blue', 2: 'green'}
    
    for i, mode_lbl in enumerate(['dégradé', 'bruité'], 1):
        xlst = [e[0] for e in Mode[i]]
        ylst = [e[1] for e in Mode[i]]
        
        ax = plt.subplot(sp[i])
        ax.set_ylim([0, 100.5])
        plt.plot(xlst, ylst, color=col[i], label=mode_lbl)
        
        plt.ylabel('accuracy')
        plt.xlabel('noize quant.')
        plt.legend(loc='lower left')
    
    plt.suptitle('train. vig.={:g}, retr. vig.={:g}'.format(tr_vig, ret_vig))
    plt.show()
