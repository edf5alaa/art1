#include <iostream>
#include <fstream>
#include <algorithm>
#include <cassert>

#include "tests.hpp"


using namespace std;


// globals
double TR_VIGIL;
double RE_VIGIL;
unordered_map<string, vector<bitset<N>>> DATA;

/*
template <size_t K>
size_t hamming_dist(const bitset<K>& bs1, const bitset<K>& bs2)
{
    return (bs1 ^ bs2).count();
}
*/

void load_data()
{
    ifstream ifs;
    ifs.open ("./data/mush-vec.data", std::ifstream::in);

    while (!ifs.eof()) {
        string categ, vec;
        ifs >> categ >> vec;
        if ((ifs.rdstate() & std::ifstream::failbit) != 0)
            break;
        DATA[categ].push_back(bitset<N>(vec));
    }

    ifs.close();
}

// ajoute du bruit, uniquement en passant des bits à 0
bitset<N> noize1 (const bitset<N>& orig, size_t k)
{
    assert(k <= orig.count());
    vector<size_t> idx;
    for (size_t i=0; i < N; ++i)
        if (orig[i]) idx.push_back(i);
    random_shuffle(idx.begin(), idx.end());

    bitset<N> bs = orig;
    for (size_t i=0; i<k; ++i)
        bs.flip(idx[i]);
    //assert (hamming_dist(orig, bs) == k);
    return bs;    
}

// ajoute du bruit en inversant certains bits
bitset<N> noize2 (const bitset<N>& orig, size_t k)
{
    assert(k <= N);
    vector<size_t> idx(N);
    for (size_t i=0; i < N; ++i)
        idx[i] = i;
    random_shuffle(idx.begin(), idx.end());
    
    bitset<N> bs = orig;
    for (size_t i=0; i<k; ++i)
        bs.flip(idx[i]);        
    //assert (hamming_dist(orig, bs) == k);
    return bs;
}

void run_tests(PredART<N>& part, int mode, size_t k)
{
    size_t n = 0, fails = 0, unkowns = 0;
    for (auto it = DATA.begin(); it != DATA.end(); ++it)
    {
        string categ = it->first;
        for (auto itt = it->second.begin(); itt != it->second.end(); ++itt)
        {
            string test;
            if (mode == 0)
                test = part.retrieve(*itt, RE_VIGIL);
            else if (mode == 1)
                test = part.retrieve(noize1(*itt, k), RE_VIGIL);
            else if (mode == 2)
                test = part.retrieve(noize2(*itt, k), RE_VIGIL);                

            if (test == "UNKOWN")
                unkowns++;
            else if (test != categ)
                fails++;
            n++;
        }
    }
    cout << "{ 'tr_vig': " << TR_VIGIL << ", 'ret_vig': " << RE_VIGIL << ", "
         << "'mode': (" << mode << "," << k << "), 'ntests': " << n << ", "
         << "'nfails': " << fails << ", 'nuknowns': " << unkowns << " }" << endl;     
}
