#include <iostream>
#include <fstream>
#include <cstdlib>

#include "tests.hpp"


using namespace std;


void usage (const char* name)
{
    cerr << "Usage: " << name << " <training-vigilance> <retrieving-vigilance>" << endl;
    exit(1);
}

int main(int argc, char * argv[])
{
    if (argc != 3)
        usage(*argv);
    
    TR_VIGIL = atof(argv[1]);
    RE_VIGIL = atof(argv[2]);

    if (TR_VIGIL < 0. or TR_VIGIL > 1. or RE_VIGIL < 0. or RE_VIGIL > 1.) {
        cerr << "Incorrect value: <vigilance> must be set between 0 .. 1" << endl;
        exit(1);
    }
    
    PredART<N> part(TR_VIGIL);
    
    load_data();
    
    for (auto it = DATA.begin(); it != DATA.end(); ++it)
        part.add_association(it->first, it->second);
    
    // mode normal
    run_tests(part, 0, 0);

    // mode noize1
    for (int i = 0; i <= 20; ++i)
        run_tests(part, 1, i);

    // mode noize2
    for (int i = 0; i <= 60; i += 2)
        run_tests(part, 2, i);

    return 0;
}
