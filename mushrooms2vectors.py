#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
from collections import defaultdict
from itertools import count

N = 22


def get_data(filename):
    with open(filename, 'r') as fp:
        return [elem for elem in [line.strip().split(',') for line in fp] 
                        if len(elem) == N + 1]

def make_vec(instance, dct):
    ret = [0] * len(dct)
    for i, attr in enumerate(instance):
        if attr == '?': continue
        ret[dct[i, attr]] = 1
    return ret


def get_vectors(data):
    domains = [set() for _ in range(N)]
    
    for categ, *inst in data:
        for s, e in zip(domains, inst):
            if e != '?': s.add(e)    

    # print(sum(len(s) for s in domains), 'attributes in total')

    gen = count()
    dct = {(i, attr): next(gen) for i, s in enumerate(domains) for attr in s}

    return [(categ, make_vec(inst, dct)) for categ, *inst in data]


if __name__ == '__main__':
    if len(sys.argv) != 2:
        print('usage:', sys.argv[0], '<infile>', file=sys.stderr)
        exit(1)

    data = get_data(sys.argv[1])
    for categ, vec in get_vectors(data):
        print(categ, ''.join(str(v) for v in vec))
