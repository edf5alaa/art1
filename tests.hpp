#ifndef __TESTS__HPP__
#define __TESTS__HPP__

#include "pred_art.hpp"

#include <vector>
#include <bitset>
#include <unordered_map>
#include <string>

#define N   116


// globals
extern double TR_VIGIL;
extern double RE_VIGIL;
extern std::unordered_map<std::string, std::vector<std::bitset<N>>> DATA;


std::bitset<N> noize1 (const std::bitset<N>& orig, size_t k);
std::bitset<N> noize2 (const std::bitset<N>& orig, size_t k);

void load_data();
void run_tests(PredART<N>& part, int mode, size_t k);

#endif
