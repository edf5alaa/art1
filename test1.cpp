#include <iostream>
#include "tests.hpp"

using namespace std;

int main()
{    
    load_data();
        
    for (float trv = 0.3; trv < 1.0001; trv += 0.0025) {
        PredART<N> part(trv);
        
        for (auto it = DATA.begin(); it != DATA.end(); ++it)
            part.add_association(it->first, it->second);
        
        cout << trv << ": " << part.nb_attractors() << endl;
    }

    return 0;
}
