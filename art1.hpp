#ifndef __ART_1__HPP__
#define __ART_1__HPP__

#include <vector>
#include <bitset>
#include <algorithm>
#include <utility>
#include <cassert>

//#define VERBOSE 1

#if VERBOSE
#include <iostream>
#endif

template <size_t N>
class ART1
{
public:
    ART1(double vigil=0.5) :
        M(0),
        vigilance(vigil),
        initB(1. / (N + 1))
    {
        assert (vigil >= 0. and vigil <= 1.);
        newstar();
    }

    bool process(const std::vector<std::bitset<N>>& inputs);
    void run(const std::vector<std::bitset<N>>& inputs);
    inline size_t nb_attractors() const { return M; }
    double retrieve(const std::bitset<N>& vec, double vigilance) const;
    
private:
    size_t newstar();
    void update(int, const std::bitset<N>&, double);

    size_t M;
    double vigilance;
    double initB;
    std::vector<std::bitset<N>> T;
    std::vector<std::vector<double>> B;
};

template <size_t N>
void ART1<N>::run(const std::vector<std::bitset<N>>& inputs)
{
#if VERBOSE
    std::cout << "  training data... (" << inputs.size() << " items)" << std::endl;
#endif
    int run = 1;
    std::vector<std::bitset<N>> input_copy = inputs;
    for (;;)
    {
#if VERBOSE
        std::cout << "    pass " << run << std::endl;
#endif
        random_shuffle(input_copy.begin(), input_copy.end());
        bool stable = process(input_copy);
        if (stable) break;
        ++run;
    }
#if VERBOSE
    std::cout << "  used " << M << " attractors" << std::endl;
#endif
}

template <size_t N>
double ART1<N>::retrieve(const std::bitset<N>& X, double vigilance) const
{
    size_t SXi = X.count();

    std::vector<std::pair<size_t, double>> activations;
    activations.reserve(M);
    for (size_t j = 0; j < M; ++j) {
        double sum = 0;
        for (size_t i = 0; i < N; ++i)
            sum += B[j][i] * X[i];
        activations.push_back(std::make_pair(j, sum));
    }
    std::sort(activations.begin(), activations.end(), 
            [](const std::pair<size_t, double>& p1, const std::pair<size_t, double>& p2){
                return p1.second < p2.second; 
            });
    while (true)
    {
        size_t star;
        if (! activations.empty()) {
            star = activations.back().first;
            activations.pop_back();
        } else {
            return -1.;
        }
        std::bitset<N> C;
        for (size_t i = 0; i < N; ++i)
            C[i] = T[star][i] & X[i];
        size_t SCi = C.count();
        double ratio = (double) SCi / SXi;
        if (ratio < vigilance)
            continue;
        return ratio;
    }
}

template <size_t N>
bool ART1<N>::process(const std::vector<std::bitset<N>>& inputs)
{
    bool stable = true;
    for (const std::bitset<N>& X: inputs)
    {
        size_t SXi = X.count();
        
        std::vector<std::pair<size_t, double>> activations;
        activations.reserve(M);
        for (size_t j = 0; j < M; ++j)
        {
            double sum = 0;
            for (size_t i = 0; i < N; ++i)
                sum += B[j][i] * X[i];
            activations.push_back(std::make_pair(j, sum));
        }
        std::sort(activations.begin(), activations.end(), 
            [](const std::pair<size_t, double>& p1, const std::pair<size_t, double>& p2){
                return p1.second < p2.second; 
            });
        bool resonnance = false;
        while (!resonnance)
        {
            size_t star;
            if (! activations.empty())
            {
                star = activations.back().first;
                activations.pop_back();
            }
            else
            {
                star = newstar();
            }
            std::bitset<N> C;
            for (size_t i = 0; i < N; ++i)
                C[i] = T[star][i] & X[i];
            size_t SCi = C.count();
            double ratio = (double) SCi / SXi;
            if (ratio < vigilance)
                continue;
            resonnance = true;
            if (T[star] != C)
            {
                update(star, C, SCi);
                stable = false;
            }
        }
    }
    return stable;
}

template <size_t N>
size_t ART1<N>::newstar()
{
    T.push_back(std::bitset<N>());
    T.back().set();
    B.push_back(std::vector<double>(N, initB));
    M += 1;
    return M - 1;
}

template <size_t N>
void ART1<N>::update(int star, const std::bitset<N>& C, double SCi)
{
    for (size_t i=0; i<N; ++i)
    {
        T[star][i] = C[i];
        B[star][i] = C[i] * (2 / (1 + SCi));
    }
}

#endif
