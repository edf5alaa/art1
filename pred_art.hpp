#include <unordered_map>
#include <string>
#include <utility>
#include <cassert>

#include "art1.hpp"

template <size_t N>
class PredART
{
public:
    PredART(double vigil) :
        vigilance(vigil)
    {}
    
    void add_association(const std::string& categ, const std::vector<std::bitset<N>>& inputs);
    std::string retrieve(const std::bitset<N>& vec, double vigil=-1) const;
    size_t nb_attractors() const;
    
private:
    std::unordered_map<std::string, ART1<N>> art1_systems;
    double vigilance;
};

template <size_t N>
void PredART<N>::add_association(const std::string& categ, const std::vector<std::bitset<N>>& inputs)
{
    assert (categ != "UNKOWN");
    art1_systems[categ] = ART1<N>(vigilance);
    art1_systems[categ].run(inputs);
}

#include <iostream>

template <size_t N>
std::string PredART<N>::retrieve(const std::bitset<N>& vec, double vigil) const
{    
    if (vigil < 0.) vigil = vigilance;
    
    std::vector<std::pair<std::string, double>> results;
    for (auto it = art1_systems.begin(); it != art1_systems.end(); ++it)
    {
        double ret = it->second.retrieve(vec, vigil);
        if (ret > 0.)
            results.push_back(std::make_pair(it->first, ret));
    }
    
    if (results.empty())
        return "UNKOWN";
    else if (results.size() == 1)
        return results.front().first;
    else
    {
        return std::max_element(results.begin(), results.end(), []
            (const std::pair<std::string, double>& p1, const std::pair<std::string, double>& p2){
                return p1.second < p2.second;
            })->first;
    }
}

template <size_t N>
size_t PredART<N>::nb_attractors() const
{
    size_t ret = 0;
    for (auto it = art1_systems.begin(); it != art1_systems.end(); ++it)
        ret += it->second.nb_attractors();
    return ret;
}
