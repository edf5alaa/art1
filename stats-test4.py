#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import matplotlib.pyplot as plt


if __name__ == '__main__':
    lst = []

    for line in sys.stdin:
        dct = eval(line)
        tr_vig, ret_vig = dct['tr_vig'], dct['ret_vig']
        nattractors, tr_sz = dct['nattractors'], dct['training_set_sz']
        ntests, nfails, nunkowns = [dct[key] for key in ['ntests', 'nfails', 'nuknowns']]
        accuracy = (1 - (nfails + nunkowns) / ntests) * 100
        lst.append((tr_sz, nattractors, accuracy))
    
    plt.figure()
    
    xlst, nattr_lst, acc_lst = zip(*lst)
    
    ax = plt.subplot(121)
    ax.plot(xlst, nattr_lst, label="nombre d'attracteurs")
    ax.set_xlabel('training data size')
    ax.set_ylabel('nb attracors used')
    ax.set_ylim(0, max(nattr_lst) + 5)
    
    ax = plt.subplot(122)
    ax.set_ylim(min(acc_lst) - 0.5, 100.1)
    ax.plot(xlst, acc_lst, label="fiabilité")
    ax.set_xlabel('training data size')
    ax.set_ylabel('accuracy')

    plt.suptitle('test4: vig.={:g}, retr. vig.={:g}'.format(tr_vig, ret_vig))
    plt.show()
