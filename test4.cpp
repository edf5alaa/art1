#include <iostream>
#include <algorithm>
#include <cstdlib>
#include "tests.hpp"

using namespace std;

void test(size_t sz)
{
    PredART<N> part(TR_VIGIL);
    
    unordered_map<string, vector<bitset<N>>> training_data;
    unordered_map<string, vector<bitset<N>>> testing_data;

    size_t tr_sz = 0;
    for (auto it = DATA.begin(); it != DATA.end(); ++it) {
        random_shuffle(it->second.begin(), it->second.end());
        
        training_data[it->first] = vector<bitset<N>>(it->second.begin(), it->second.begin() + sz);
        testing_data[it->first] = vector<bitset<N>>(it->second.begin() + sz, it->second.end());
#if VERBOSE
        cout << "training set for "<< it->first << " : " << training_data[it->first].size() << " elements" << endl;
        cout << "testing set for "<< it->first << " : " << testing_data[it->first].size() << " elements" << endl;
#endif
        tr_sz += training_data[it->first].size();
        
        part.add_association(it->first, training_data[it->first]);
    }
    
    size_t n = 0, fails = 0, unkowns = 0;
    for (auto it = testing_data.begin(); it != testing_data.end(); ++it) {
        string categ = it->first;
        for (auto itt = it->second.begin(); itt != it->second.end(); ++itt) {
            string test = part.retrieve(*itt, RE_VIGIL);
            if (test == "UNKOWN")
                unkowns++;
            else if (test != categ)
                fails++;
            n++;
        }
    }
    
    cout << "{ 'tr_vig': " << TR_VIGIL << ", 'ret_vig': " << RE_VIGIL << ", 'training_set_sz': " << tr_sz << 
        ", 'nattractors' : " << part.nb_attractors() << ", 'ntests': " << n << ", " << "'nfails': " << fails << ", 'nuknowns': " << unkowns << " }" << endl;
}

void usage (const char* name)
{
    cerr << "Usage: " << name << " <training-vigilance> <retrieving-vigilance>" << endl;
    exit(1);
}

int main(int argc, char * argv[])
{
    if (argc != 3)
        usage(*argv);
    
    TR_VIGIL = atof(argv[1]);
    RE_VIGIL = atof(argv[2]);

    if (TR_VIGIL < 0. or TR_VIGIL > 1. or RE_VIGIL < 0. or RE_VIGIL > 1.) {
        cerr << "Incorrect value: <vigilance> must be set between 0 .. 1" << endl;
        exit(1);
    }
    
    load_data();
    
    for (size_t sz = 50; sz <= 3000; sz += 50)
        test(sz);

    return 0;
}
