#include <iostream>
#include <fstream>
#include <cstdlib>
#include <algorithm>

#include "tests.hpp"


using namespace std;


void usage (const char* name)
{
    cerr << "Usage: " << name << " <mode> <noize-quant.>" << endl;
    exit(1);
}

int main(int argc, char * argv[])
{    
    if (argc != 3)
        usage(*argv);

    unsigned mode = atol(argv[1]);
    unsigned noize_quant = atol(argv[2]);
    
    assert (mode == 1 or mode == 2);

    load_data();
        
    for (float trv = 0.6; trv < 0.851; trv += 0.04) {
        TR_VIGIL = trv;
        
        PredART<N> part(trv);
        
        for (float rev = 0.; rev < min<float>(0.6, trv); rev += 0.04) {
            RE_VIGIL = rev;
            
            for (auto it = DATA.begin(); it != DATA.end(); ++it)
                part.add_association(it->first, it->second);

            run_tests(part, mode, noize_quant);
        }
    }

    return 0;
}
