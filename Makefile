CXX=g++
CXXFLAGS=-O2 -Wall -Wextra --std=c++11
LDFLAGS=

.PHONY: clean, mrproper

all: test1 test2 test3 test4

test1: test1.o tests.o
	$(CXX) -o test1 test1.o tests.o $(LDFLAGS)

test2: test2.o tests.o
	$(CXX) -o test2 test2.o tests.o $(LDFLAGS)

test3: test3.o tests.o
	$(CXX) -o test3 test3.o tests.o $(LDFLAGS)

test4: test4.o tests.o
	$(CXX) -o test4 test4.o tests.o $(LDFLAGS)

test1.o: test1.cpp tests.hpp art1.hpp  pred_art.hpp
	$(CXX) -o test1.o -c test1.cpp $(CXXFLAGS)

test2.o: test2.cpp tests.hpp art1.hpp  pred_art.hpp
	$(CXX) -o test2.o -c test2.cpp $(CXXFLAGS)
	
test3.o: test3.cpp tests.hpp art1.hpp  pred_art.hpp
	$(CXX) -o test3.o -c test3.cpp $(CXXFLAGS)

test4.o: test4.cpp tests.hpp art1.hpp  pred_art.hpp
	$(CXX) -o test4.o -c test4.cpp $(CXXFLAGS)

tests.o: tests.cpp tests.hpp art1.hpp  pred_art.hpp
	$(CXX) -o tests.o -c tests.cpp $(CXXFLAGS)

clean:
	rm -rf *.o

mrproper: clean
	rm -rf test1 test2 test3 test4
