#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys

import matplotlib.pyplot as plt


if __name__ == '__main__':    
    Mode = {i: [] for i in [0, 1, 2]}
    vals = []
    for line in sys.stdin:
        vig, nb_attractors = [float(e.strip()) for e in line.split(':')]
        vals.append((vig, nb_attractors))

    xlst, ylst = list(zip(*vals))
    plt.plot(xlst, ylst)
    
    plt.ylabel('nb. attractors used')
    plt.xlabel('training vigilance')
    
    plt.show()
